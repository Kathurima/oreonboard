from django.conf.urls import url

from boards import views as board_views

urlpatterns = [
    url(r'^$', board_views.BoardListView.as_view(), name='home'),
    url(r'^boards/(?P<pk>\d+)/$', board_views.BoardTopicListView.as_view(), name='board_topics'),
    url(r'^boards/(?P<pk>\d+)/new/$', board_views.new_topic, name='new_topic'),
    url(r'^boards/(?P<pk>\d+)/topics/(?P<topic_pk>\d+)/$', board_views.TopicPostListView.as_view(), name='topic_posts'),
    url(r'^boards/(?P<pk>\d+)/topics/(?P<topic_pk>\d+)/reply/$', board_views.reply_topic, name='reply_topic'),
    url(r'^boards/(?P<pk>\d+)/topics/(?P<topic_pk>\d+)/posts/(?P<post_pk>\d+)/edit/$',
        board_views.PostUpdateView.as_view(), name='edit_post'),
]

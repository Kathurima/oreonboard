from django import forms
from .models import Topic, Post
from django.utils.html import mark_safe
from markdown import markdown


class NewTopicForm(forms.ModelForm):
    message = forms.CharField(
        widget=forms.Textarea(
            attrs={'rows': 5, 'placeholder': 'What is on your mind?'}
        ),
        max_length=10000,
        help_text='The max length of the text is 10000.'
    )

    class Meta:
        model = Topic
        fields = ['subject', 'message']

    def get_message_as_markdown(self):
        return mark_safe(markdown(self.message, safe_mode='escape'))


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['message', ]
